<?php
$I = new FunctionalTester($scenario);

$user = array(
  'email' => 'drake.aren-buyer@protonmail.com',
  'password' => '^D*^469^u4*65^*!938&'
);

$I->amOnPage('/?page_id=6&1cu_test=27:1');

$I->see('Proceed to checkout');
$I->click('Proceed to checkout');

$I->see('Checkout');
$I->submitForm('.checkout', array(
    'billing_first_name' => 'test',
    'billing_last_name' => 'test',
    'billing_country' => 'RU',
    'billing_state' => 'test',
    'billing_address_1' => 'test',
    'billing_address_2' => 'test',
    'billing_city' => 'test',
    'billing_postcode' => 'test',
    'billing_phone' => 'test',
    'billing_email' => $user['email'],
    'payment_method' => 'ocupaypal'
));
$I->click('Place order');

$I->see('Pay with PayPal');
$I->seeInCurrentUrl('sandbox');
$I->submitForm('.checkout', array(
  'login_email' => $user['email'],
  'login_password' => $user['password'],
));
$I->click('Log In');

$I->see('Agree & Pay');
$I->click('Agree & Pay');

$I->see('Buy Now');
$I->click('Buy Now');

$I->see('Order received');
$I->seeInCurrentUrl('added');