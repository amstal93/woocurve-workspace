DATABASE_NAME=wordpress
DATABASE_HOST=localhost
DATABASE_UESR=root
DATABASE_UESR_PASSWORD=password

init: download-wordpress setup-wordpress setup-database
	composer install
	git submodule init
	git submodule update --recursive --remote

download-wordpress:
	rm -Rf domain/*
	curl https://wordpress.org/latest.tar.gz -o domain/wordpress.tar.gz
	tar -xf domain/wordpress.tar.gz -C domain
	mv domain/wordpress/* domain/
	rm -Rf domain/wordpress.tar.gz domain/wordpress

setup-wordpress-config:
	cp wp-config.php domain/wp-config.php

setup-database:
	mysql \
		-h ${DATABASE_HOST} \
		-u ${DATABASE_UESR} \
		--password=${DATABASE_UESR_PASSWORD} \
		--execute='CREATE DATABASE wordpress; GRANT ALL PRIVILEGES ON wordpress.* TO "wpuser"@"localhost" IDENTIFIED BY "wpuser"; FLUSH PRIVILEGES;'

serve:
	php -S localhost:40080 -t domain/ -c php.ini

build:
	rm -Rf release/*
	cd src/gb-wc-1cu && zip -r ../../release/gb-wc-1cu.zip ./*
	cd src/gb-wc-hcc && zip -r ../../release/gb-wc-hcc.zip ./*

sync:
	cp -r src/gb-wc-1cu/* domain/wp-content/plugins/gb-wc-1cu
	cp -r src/gb-wc-hcc/* domain/wp-content/plugins/gb-wc-hcc

wp-shell:
	wp shell --path="./domain/"

db-backup:
	mysqldump \
		--add-drop-table \
		-h ${DATABASE_HOST} \
		-u ${DATABASE_UESR} \
		--password=${DATABASE_UESR_PASSWORD} \
		${DATABASE_NAME} > dumps/main.sql
	rm tests/_data/dump.sql
	ln -sr dumps/main.sql tests/_data/dump.sql

db-restore:
	mysql \
		-h ${DATABASE_HOST} \
		-u ${DATABASE_UESR} \
		--password=${DATABASE_UESR_PASSWORD} \
		${DATABASE_NAME} < dumps/main.sql

db-shell:
	mysql \
		-h ${DATABASE_HOST} \
		-u ${DATABASE_UESR} \
		--password=${DATABASE_UESR_PASSWORD}

test: db-backup
	codecept run

test-acceptance: db-backup
	codecept run acceptance

test-functional: db-backup
	codecept run functional

test-unit: db-backup
	codecept run unit

test-wpunit: db-backup
	codecept run wpunit

logs: db-logs wp-logs

db-logs:
	mysql \
		-h ${DATABASE_HOST} \
		-u ${DATABASE_UESR} \
		--password=${DATABASE_UESR_PASSWORD} \
		--execute='use wordpress; select * from wp_failed_jobs; select * from wp_woocommerce_log;'

wp-logs:
	tail -f domain/wp-content/debug.log
